const AllFruits = (props) => {
  var fruits = props.fruits.map((fruit) => {
      return(
        <div key={fruit.id}>
          <Fruit fruit={fruit} handleDelete={props.handleDelete} handleUpdate={props.handleUpdate}/>
        </div>
      )
    })
  return(
        <div>
          <h1>Beginning of AllFruits Component</h1>
          {fruits}
          <h1>End of AllFruits Component</h1>
        </div>
      )
  }
